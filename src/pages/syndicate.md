---
layout: ../layouts/MarkdownLayout.astro
title: XLabber Investment Club
---

# XLabber Investment Club Syndicate*

A syndicate fueled by the talent, expertise, and passion of former GitLab team members.

<a href="https://angellist.com/s/xlabber-ic/nCfkJ" class="button">See our syndicate on AngelList</a>

## Leads

Our syndicate is led by [**Brendan**](https://www.linkedin.com/in/olearycrew/) and [**Emilie**](https://www.linkedin.com/in/emilieschario/).

However, we use the term "lead" modestly - we're all collaborators here. And the best part? We charge 0% carry.

## Limited Partners (LPs)

We welcome all former GitLab team members to be part of our syndicate. To apply, simply [submit your application](https://angellist.com/s/xlabber-ic/nCfkJ) along with your LinkedIn profile for a quick turn around. Our LPs include a diverse group of individuals, from founders and executives to individual contributors. They bring a range of perspectives and experiences that enrich our syndicate.

## Investment Thesis

The XLabber Investment Club is built on the same [values that build GitLab](https://handbook.gitlab.com/handbook/values/) – CREDIT: Collaboration, Results, Efficiency, Diversity, Iteration, and Transparency. We seek to build these same values in the next generation of startups through funding and advising. Thus we can extend the things we learned at GitLab that we [don't want to forget](https://boleary.dev/what-i-learned-at-gitlab-that-i-dont-want-to-forget/). 

Our competitive advantage is that we get exclusive deal flow based on the network of GitLabbers - former GitLab team members who still embody GitLab’s values.

## Qualification

- XLabbers and XLabber-adjacent companies are eligible to pitch the syndicate. 
- An XLabber is someone who worked for GitLab at some point in time. 
- A company that an XLabber works for, is friendly with the founder of, or has some other relationship that means they want to bring them to the syndicate is fine.

## Getting Funded

Our funding process is as follows:

1. Join the XLabber slack group to DM or email the Leads.
2. Provide access to necessary data including a pitch deck and a recorded ~5min pitch.
3. XLabber will then write a deal memo in the prescribed format.
4. Provide additional input as needed.
5. We will set up the deal in AngelList and submit it for review.
6. If approved by AngelList, we will then send the deal to our LPs to gather interest.
7. If the minimum investment is met, AngelList will provide closing documentation and wire information.

Each partnership will be named "`XLabbers x [Company Name]`".

## Investment Guidelines

We follow the guidelines below to ensure our investments are strategic and beneficial for all parties:

- We only do follow-on investments, with an existing lead.
- Each investment requires a minimum allocation of $75k.
- The investment will not proceed if the minimum of $75k (plus fees) is not reached.
- We invest in preferred shares and don't ask for pro-rata rights.
- We prefer investing in Delaware C-corporations.

## More Information

- [How AngelList syndicates work](https://www.angellist.com/syndicates)

---

*\*This syndicate is not affiliated with GitLab, Inc.*
